package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class AboutTest {

	@Test
	public void testAbout() throws Exception {
		String description = new About().desc();
        assertTrue("Description", description.contains("application was copied"));
	}

	@Test
	public void testCalcmul() throws Exception {
		int res = new Calcmul().mul();
		assertEquals("Multiplication", 18, res);
	}

	@Test
	public void testAddition() throws Exception {
		int res = new Calculator().add();
		assertEquals("Addition", 9, res);
	}

	@Test
	public void testCnt() throws Exception {
		int res = new Counter().divide(10,2);
		assertEquals("Division", 5, res);
	}

		@Test
	public void testCntZero() throws Exception {
		int res = new Counter().divide(10,0);
		assertEquals("Division with zero", Integer.MAX_VALUE, res);
	}

}
