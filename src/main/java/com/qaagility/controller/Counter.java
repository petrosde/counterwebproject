package com.qaagility.controller;

public class Counter {

    public int divide(int numberA, int numberB) {
        if (numberB == 0) {
            return Integer.MAX_VALUE;
        }
        else {
            return numberA / numberB;
        }
    }

}
